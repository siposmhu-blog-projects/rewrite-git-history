public class Calculator
{
    // comment about addition method...
    public int Add(int a, int b)
    {
        return a + b;
    }

    // comment about subtraction method...
    public int Sub(int a, int b)
    {
        return a - b;
    }

    // comment about multiplication method...
    public object Mul(int a, int b)
    {
        return a * b;
    }

    // comment about division method...
    public object Div(int a, int b)
    {
        if(b == 0)
            throw new Exception("zero can't be a divider");
        else
            return a / b;
    }
}